# Add here internal Software Heritage dependencies, one per line.
swh.core[http] >= 0.3  # [http] is required by swh.core.pytest_plugin
swh.loader.git >= 1.4.0
swh.model >= 5.0.0
swh.storage >= 1.1.0
swh.journal >= 0.9.0
